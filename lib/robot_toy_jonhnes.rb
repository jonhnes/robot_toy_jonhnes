require "robot_toy_jonhnes/version"
require "robot_toy_jonhnes/game"
require "robot_toy_jonhnes/position_validator"
require "robot_toy_jonhnes/robot_mover"

module RobotToyJonhnes
  class Error < StandardError; end
end
