module RobotToyJonhnes
  class Game
    CARDINAL_POINTS = %i[NORTH EAST SOUTH WEST].freeze
    attr_accessor :position_line, :position_column, :robot_height, :robot_width,
                  :facing, :command

    def initialize
      @position_line = @position_column = 0
      @robot_height = @robot_width = 1
      @facing = :EAST
    end

    def board_height
      @robot_height * 5
    end

    def board_width
      @robot_width * 5
    end

    def place(new_place)
      new_line, new_column, new_orientation = new_place.split(',')
      RobotMover.new(self).to_local new_line, new_column, new_orientation
    end

    def report
      board_print = "\n"
      board_height.times { |index| board_print += game_line(index) + "\n" }
      limit_line + board_print + limit_line
    end

    def rotate_left
      facing_index = CARDINAL_POINTS.find_index @facing
      @facing = CARDINAL_POINTS[facing_index - 1]
    end

    def rotate_right
      facing_index = CARDINAL_POINTS.find_index @facing
      @facing = CARDINAL_POINTS[facing_index + 1] || CARDINAL_POINTS[0]
    end

    def move
      movement_options = {NORTH: :to_north, SOUTH: :to_south,
                          WEST: :to_west, EAST: :to_east}
      RobotMover.new(self).send movement_options[@facing]
    end

    private

    def limit_line
      '#' * (board_width + 2)
    end

    def robot_in_line?(line)
      last_line_position = robot_width + position_line - 1
      (position_line..last_line_position).to_a.include? line
    end

    def robot_column_indexes
      last_column_position = position_column + robot_width - 1
      position_column..last_column_position
    end

    def game_line(line)
      line_draw = '-' * board_width
      line_draw[robot_column_indexes] = facing[0] if robot_in_line? line
      '#' + line_draw + '#'
    end
  end
end