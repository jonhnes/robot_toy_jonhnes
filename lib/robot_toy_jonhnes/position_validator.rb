module RobotToyJonhnes
  class PositionValidator
    def self.new_position_valid?(new_position, board_limit:)
      hit_top = new_position.to_i.negative?
      hit_bottom = new_position.to_i >= board_limit
      !hit_top && !hit_bottom
    end

    def self.new_place_valid?(new_line, new_column, orientation, game)
      line_valid = new_position_valid? new_line, board_limit: game.board_width
      column_valid = new_position_valid? new_column, board_limit: game.board_height
      face_valid = Game::CARDINAL_POINTS.include? orientation&.upcase&.to_sym

      line_valid && column_valid && face_valid
    end
  end
end