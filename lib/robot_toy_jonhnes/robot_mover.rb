module RobotToyJonhnes
  class RobotMover
    attr :game

    def initialize(game)
      @game = game
    end

    def to_north
      new_line_position = game.position_line - 1
      update_line_position new_line_position
    end

    def to_south
      new_line_position = game.position_line + 1
      update_line_position new_line_position
    end

    def to_west
      new_column_position = game.position_column - 1
      update_column_position new_column_position
    end

    def to_east
      new_column_position = game.position_column + 1
      update_column_position new_column_position
    end

    def to_local(new_line, new_column, new_orientation)
      can_save = PositionValidator.new_place_valid? new_line, new_column,
                                                    new_orientation, game
      raise StandardError, 'Invalid place' unless can_save

      game.position_line = new_line.to_i
      game.position_column = new_column.to_i
      game.facing = new_orientation.upcase.to_sym
    end

    private

    def update_line_position(new_position)
      can_save = PositionValidator.new_position_valid?(
        new_position, board_limit: game.board_height
      )
      raise StandardError, 'You reached the border' unless can_save

      game.position_line = new_position.to_i
    end

    def update_column_position(new_position)
      can_save = PositionValidator.new_position_valid?(
        new_position, board_limit: game.board_width
      )
      raise StandardError, 'You reached the border' unless can_save

      game.position_column = new_position.to_i
    end
  end
end