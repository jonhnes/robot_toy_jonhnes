RSpec.describe RobotToyJonhnes::Game do
  let(:game) { described_class.new }
  describe 'default values' do
    subject { game }

    it do
      expect(subject.position_line).to eq 0
      expect(subject.position_column).to eq 0
      expect(subject.robot_height).to eq 1
      expect(subject.robot_width).to eq 1
      expect(subject.facing).to eq :EAST
    end

  end

  describe '.place' do
    let(:new_line) { rand(1..5).to_s }
    let(:new_column) { rand(1..5).to_s }
    let(:new_orientation) { RobotToyJonhnes::Game::CARDINAL_POINTS.sample.to_s }
    let(:new_place) { [new_line, new_column, new_orientation].join(',') }
    let(:mover) { double }

    after { game.place new_place }

    it do
      expect(RobotToyJonhnes::RobotMover).to receive(:new).with(game) { mover }
      expect(mover).to receive(:to_local).with(new_line, new_column, new_orientation)
    end
  end

  describe 'report' do
    let(:report_by_line) { subject.split("\n") }
    let(:first_line) { report_by_line.first }
    let(:last_line) { report_by_line.last }
    let(:robot_location) do
      report_by_line[game.position_line + 1][game.position_column + 1]
    end
    subject { game.report }

    it do
      expect(first_line).to eq '#' * (game.board_width + 2)
      expect(last_line).to eq '#' * (game.board_width + 2)
      expect(robot_location).to eq game.facing[0]
    end
  end

  describe '.rotate_left' do

    subject { game.rotate_left }

    context 'when in NORTH face' do
      before do
        game.facing = :NORTH
        subject
      end

      it do
        expect(game.facing).to eq :WEST
      end
    end

    context 'when in EAST face' do
      before do
        game.facing = :EAST
        subject
      end

      it do
        expect(game.facing).to eq :NORTH
      end
    end

    context 'when in SOUTH face' do
      before do
        game.facing = :SOUTH
        subject
      end

      it do
        expect(game.facing).to eq :EAST
      end
    end

    context 'when in WEST face' do
      before do
        game.facing = :WEST
        subject
      end

      it do
        expect(game.facing).to eq :SOUTH
      end
    end
  end

  describe '.rotate_right' do

    subject { game.rotate_right }

    context 'when in NORTH face' do
      before do
        game.facing = :NORTH
        subject
      end

      it do
        expect(game.facing).to eq :EAST
      end
    end

    context 'when in EAST face' do
      before do
        game.facing = :EAST
        subject
      end

      it do
        expect(game.facing).to eq :SOUTH
      end
    end

    context 'when in SOUTH face' do
      before do
        game.facing = :SOUTH
        subject
      end

      it do
        expect(game.facing).to eq :WEST
      end
    end

    context 'when in WEST face' do
      before do
        game.facing = :WEST
        subject
      end

      it do
        expect(game.facing).to eq :NORTH
      end
    end
  end

  describe '.move' do

    subject { game.move }

    context 'when facing NORTH' do
      before do
        game.facing = :NORTH
        game.position_column = game.position_line = 2
        subject
      end

      it do
        expect(game.position_line).to eq 1
        expect(game.position_column).to eq 2
      end
    end

    context 'when facing SOUTH' do
      before do
        game.facing = :SOUTH
        game.position_column = game.position_line = 2
        subject
      end

      it do
        expect(game.position_line).to eq 3
        expect(game.position_column).to eq 2
      end
    end

    context 'when facing EAST' do
      before do
        game.facing = :EAST
        game.position_column = game.position_line = 2
        subject
      end

      it do
        expect(game.position_line).to eq 2
        expect(game.position_column).to eq 3
      end
    end

    context 'when facing WEST' do
      before do
        game.facing = :WEST
        game.position_column = game.position_line = 2
        subject
      end

      it do
        expect(game.position_line).to eq 2
        expect(game.position_column).to eq 1
      end
    end
  end
end