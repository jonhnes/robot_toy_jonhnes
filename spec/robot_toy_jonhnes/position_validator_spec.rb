RSpec.describe RobotToyJonhnes::PositionValidator do
  describe '.new_position_valid?' do
    subject do
      described_class.new_position_valid?(new_position,
                                            board_limit: board_limit)
    end

    context 'when not reached the board border' do
      let(:board_limit) { rand(5..10) }
      let(:new_position) { rand(0..(board_limit-1)) }

      it do
        is_expected.to be_truthy
      end
    end

    context 'when already reached the top or left of the board ' do
      let(:new_position) { -1 }
      let(:board_limit) { rand(5..10) }

      it do
        is_expected.to be_falsey
      end
    end

    context 'when already reached the bottom or right of the board ' do
      let(:new_position) { board_limit }
      let(:board_limit) { rand(5..10) }

      it do
        is_expected.to be_falsey
      end
    end
  end

  describe '.new_place_valid?' do
    let(:game) { double board_width: board_width, board_height: board_height }
    let(:board_height) { rand(1..10) }
    let(:board_width) { rand(1..10) }

    subject do
      described_class.new_place_valid? new_line, new_column,
                                         new_orientation, game
    end

    context 'when everything its ok' do
      let(:new_line) { rand(0..(board_width-1)) }
      let(:new_column) { rand(0..(board_height-1)) }
      let(:new_orientation) { RobotToyJonhnes::Game::CARDINAL_POINTS.sample }

      it do
        is_expected.to be_truthy
      end
    end

    context 'when line invalid' do
      let(:new_line) { rand(board_width..(board_width + 10)) }
      let(:new_column) { rand(0..(board_height-1)) }
      let(:new_orientation) { RobotToyJonhnes::Game::CARDINAL_POINTS.sample }

      it do
        is_expected.to be_falsey
      end
    end

    context 'when column invalid' do
      let(:new_line) { rand(0..(board_width-1)) }
      let(:new_column) { rand(board_height..(board_height + 10)) }
      let(:new_orientation) { RobotToyJonhnes::Game::CARDINAL_POINTS.sample }

      it { is_expected.to be_falsey }
    end


    context 'when orientation invalid' do
      let(:new_line) { rand(0..(board_width - 1)) }
      let(:new_column) { rand(0..(board_height - 1)) }
      let(:new_orientation) { (:a..:z).to_a.sample }

      it { is_expected.to be_falsey }
    end
  end
end