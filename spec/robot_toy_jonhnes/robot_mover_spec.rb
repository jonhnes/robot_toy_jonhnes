RSpec.describe RobotToyJonhnes::RobotMover do
  let(:game) { RobotToyJonhnes::Game.new }
  let(:mover) { RobotToyJonhnes::RobotMover.new game }
  let(:validator_class) { RobotToyJonhnes::PositionValidator }
  let(:error_msg) { 'You reached the border' }

  describe '.to_north' do
    subject { mover.to_north }

    context 'when position invalid' do
      before do
        allow(validator_class).to receive(:new_position_valid?) { false }
      end

      it { expect { subject }.to raise_error StandardError, error_msg }
    end

    context 'when position valid' do
      let(:old_position) { rand(1..5) }
      before do
        game.position_line = old_position
        allow(validator_class).to receive(:new_position_valid?) { true }
      end

      it { is_expected.to eq old_position - 1 }
    end
  end

  describe '.to_south' do
    subject { mover.to_south }

    context 'when position invalid' do
      before do
        allow(validator_class).to receive(:new_position_valid?) { false }
      end

      it { expect { subject }.to raise_error StandardError, error_msg }
    end

    context 'when position valid' do
      let(:old_position) { rand(1..5) }
      before do
        game.position_line = old_position
        allow(validator_class).to receive(:new_position_valid?) { true }
      end

      it { is_expected.to eq old_position + 1 }
    end
  end

  describe '.to_west' do
    subject { mover.to_west }

    context 'when position invalid' do
      before do
        allow(validator_class).to receive(:new_position_valid?) { false }
      end

      it { expect { subject }.to raise_error StandardError, error_msg }
    end

    context 'when position valid' do
      let(:old_position) { rand(1..5) }
      before do
        game.position_column = old_position
        allow(validator_class).to receive(:new_position_valid?) { true }
      end

      it { is_expected.to eq old_position - 1 }
    end
  end

  describe '.to_east' do
    subject { mover.to_east }

    context 'when position invalid' do
      before do
        allow(validator_class).to receive(:new_position_valid?) { false }
      end

      it { expect { subject }.to raise_error StandardError, error_msg }
    end

    context 'when position valid' do
      let(:old_position) { rand(1..5) }
      before do
        game.position_column = old_position
        allow(validator_class).to receive(:new_position_valid?) { true }
      end

      it { is_expected.to eq old_position + 1 }
    end
  end

  describe 'to_local' do
    let(:new_line) { rand(1..5).to_s }
    let(:new_column) { rand(1..5).to_s }
    let(:new_facing) { RobotToyJonhnes::Game::CARDINAL_POINTS.sample.to_s }

    subject { mover.to_local new_line, new_column, new_facing }

    context 'when local is invalid' do
      before do
        allow(validator_class).to receive(:new_place_valid?)
          .with(new_line, new_column, new_facing, game).and_return(false)
      end

      it do
        expect { subject }.to raise_error(StandardError, 'Invalid place')
      end
    end

    context 'when local is valid' do
      before do
        allow(validator_class).to receive(:new_place_valid?)
          .with(new_line, new_column, new_facing, game).and_return(true)
      end

      it do
        expect(game).to receive(:position_line=).with(new_line.to_i)
        expect(game).to receive(:position_column=).with(new_column.to_i)
        expect(game).to receive(:facing=).with(new_facing.to_sym)
        subject
      end
    end
  end
end